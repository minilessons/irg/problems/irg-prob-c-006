

class PolygonProblem {
	constructor(window, wrapper, maxAxis, width, height) {
		const dimensionProvider = () => Object.assign({ width: width || 500, height: height || 500 });
		const wrapperDimensions = dimensionProvider();
		const renderer = getRenderer(wrapperDimensions);
		const scene = new THREE.Scene();
		const camera = default2DCamera(PIXELS_PER_UNIT, wrapperDimensions);

		const grid = construct2DGrid(maxAxis, CENTER_LINE_COLOR, GRID_COLOR);
		const snapToGrid = false;

		wrapper.appendChild(renderer.domElement);
		scene.add(grid);

		const movementHandler = new ScreenMovementHandler(renderer, camera, 
			RIGHT_MOUSE_BUTTON);
		wrapper.addEventListener('mousedown', movementHandler.onMousedown);
		wrapper.addEventListener('mouseup', movementHandler.onMouseup);
		wrapper.addEventListener('mousemove', movementHandler.onMousemove);
		wrapper.addEventListener('wheel', movementHandler.onMousewheel);
		wrapper.oncontextmenu = e => e.preventDefault();
		wrapper.onwheel = e => e.preventDefault();

		const intersectionFinder = new IntersectionFinder(camera, 
			() => renderer.context.canvas.getBoundingClientRect(), new THREE.Raycaster());
		const helperLinesHandler = new HelperLinesHandler(snapToGrid, 
			grid, intersectionFinder, wrapper, scene, maxAxis);
		
		function createTextDisplay(x, y, text) {
			const positionDataElement = document.createElement("div");
			positionDataElement.style = "position: absolute;" 
				+ "top: " + y + "px; left: " + x + "px; z-index: 102; display:block; "
				+ "background: white; padding: 10px";
			positionDataElement.innerHTML = text;
			return positionDataElement;
		}

		const edgeDisplay = createTextDisplay(0, 0, "");
		wrapper.appendChild(edgeDisplay);

		const updateEdgeDisplay = (i, x, y, e) => {
			edgeDisplay.innerHTML = i.object.parent.userData.name;
			edgeDisplay.style.visibility = "visible";
			edgeDisplay.style.left = (e.pageX - 20) + "px";
			edgeDisplay.style.top = (e.layerY + 10) + "px";
		};

		const selection = new Selection(() => scene.children, 
										els => els.filter(o => o.object.parent.userData.name),
										() => edgeDisplay.style.visibility = "hidden",
										updateEdgeDisplay,
										intersectionFinder);

		wrapper.addEventListener('mousemove', helperLinesHandler.onMousemove);
		wrapper.addEventListener('mousemove', selection.onMousemove);
		wrapper.appendChild(helperLinesHandler.positionDataElement);

		this.resize = onWindowResize(PIXELS_PER_UNIT, 
			dimensionProvider, intersectionFinder, camera, renderer);

		window.addEventListener('resize', this.resize, false);

		this.scene = scene;
		this.renderer = renderer;
		this.camera = camera;
		this.wrapper = wrapper;
		this.dimensionProvider = dimensionProvider;

		this.animate = this.animate.bind(this);
	}

	animate() {
		function isHidden(el) {
			return (el.offsetParent === null)
		}

		requestAnimationFrame(this.animate);
		this.renderer.render(this.scene, this.camera);

		if (!isHidden(this.wrapper)) {
		} else {
		}
	}
}

