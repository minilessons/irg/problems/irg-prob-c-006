
/*@#common-include#(three.js)*/
/*@#common-include#(Constants.js)*/
/*@#common-include#(Helpers.js)*/
/*@#common-include#(IntersectionFinder.js)*/
/*@#common-include#(Display2D.js)*/
/*@#common-include#(HelperLinesHandler.js)*/
/*@#common-include#(ScreenMovementHandler.js)*/
/*@#common-include#(ElementCreation.js)*/
/*@#common-include#(Selection.js)*/
/*@#include#(PolygonProblem.js)*/

/**
 *	cst:
 *		points 		:: [vec2] - tocke poligona
 *		testPoint :: vec2 - tocka koju testiramo s obzirom na stranice poligona
 *	st:
 *		disabled 	:: boolean - je li korisnik submittao odgovor (ako da, onemoguci forme)
 * 		qs				:: [[value]] - lista korisnikovih odgovora u obliku matrice,
 * 								 ove vrijednosti ispunjavaju formu
 */
var api = {
	initQuestion: function(cst, st) {
		const problem = new PolygonProblem(window, cst.maxAxis,
			document.getElementById(questionLocalID + "_wrap"), cst.canvasWidth, cst.canvasHeight);

		function generateEdgeNames(points) {
			const edgePrefix = "E";
			return points.map((pt, i) => edgePrefix + i);
		}

		function arrows(pts, names) {
			const a = pts.map((pt, i) => arrow(pt, pts[(i+1) % pts.length], 
				LINE_COLOR_UNSELECTED));
			a.forEach((ar, i) => ar.userData.name = names[i]);
			return a;
		}

		const edgeNames = generateEdgeNames(cst.points);
		const triangleVectors = arrows(cst.points, edgeNames);
		triangleVectors.forEach(vector => problem.scene.add(vector));

		const ptMaterial = { color: POINT_COLOR_UNSELECTED };
		cst.points.forEach(ptVec => problem.scene.add(point(ptVec, ptMaterial)));

		const pt = point(cst.testPoint, ptMaterial);
		problem.scene.add(pt);

		function generateUserInputsForPoints(edgeNames) {
			const RELATIONSHIP_BELOW = [-1, "Ispod"];
			const RELATIONSHIP_ABOVE = [+1, "Iznad"];
			return edgeNames.map(name => Object.assign({ 
				name: name, 
				inputs: { a: createInput(), 
								  b: createInput(), 
									c: createInput(), 
									relationship: createChoice([RELATIONSHIP_BELOW, RELATIONSHIP_ABOVE]) }
			}));
		}

		function generateTableRowsForUserInputs(userInputs) {
			return userInputs.map(ui => {
				const row = ce("tr");
				const name = createNameElement(ui.name);
				const inputs = ui.inputs;

				[ name, inputs.a, inputs.b, inputs.c, inputs.relationship ]
					.map(el => wrapTd(el))
					.forEach(td => row.appendChild(td));

				return row;
			});
		}

		const userInputs = generateUserInputsForPoints(edgeNames);
		const tableRows = generateTableRowsForUserInputs(userInputs);

		const table = document.getElementById(questionLocalID + "_table");
		const tbody = table.getElementsByTagName("tbody")[0];
		tableRows.forEach(row => tbody.appendChild(row));

		this.takeState = () =>
			userInputs
				.map(ui => ui.inputs)
				.map(ui => Object.values(ui).map(domEl => domEl.value));
					
		const setValueDict = {
			"INPUT": (e, val) => e.value = val,
			"SELECT": (e, val) => Object.keys(e.children)
				.map(k => e.children[k])
				.filter(ch => ch.value == val)
				.forEach(ch => ch.selected = true)
		};

		this.resetState = (cst, st) =>
			userInputs
				.map(ui => ui.inputs)
				.forEach(_in => 
					Object.values(_in).forEach(domEl => 
						setValueDict[domEl.tagName](domEl, "")));

		this.revertState = (cst, st) => 
			userInputs
				.map(ui => ui.inputs)
				.forEach((_in, i) => 
					Object.values(_in).forEach((domEl, j) => 
						setValueDict[domEl.tagName](domEl, st.qs[i][j])));

		if (st.qs && st.qs.length && st.qs[0]) {
			this.revertState(cst, st);
		}

		if (st.disabled) {
			userInputs
				.map(ui => ui.inputs)
				.forEach((_in, i) => 
					Object.values(_in).forEach(domEl => 
						domEl.disabled = true));
		}

		problem.animate();
	}
};

return api;

