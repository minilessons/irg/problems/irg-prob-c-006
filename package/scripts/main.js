
/*@#common-include#(three.js)*/
/*@#include#(ServerSideHelpers.js)*/

function generateRandomPoints(nPoints, minScale, maxScale) {
	if (nPoints > 4) {
		throw 'Random generator expects max 4 points';
	}

	if (minScale > maxScale) {
		throw 'Min scale should be at most equal to max scale';
	}

	const limits = {
		minX: -1.5, rangeX: 0.5,
		minY: -1.5, rangeY: 0.5
	};

	const rotateAngle = Math.random() * 2 * Math.PI;		
	const scale = minScale + Math.random() * (maxScale - minScale);
	const zAxis = new THREE.Vector3(0, 0, 1);

	return range(nPoints).map(function(i) {
		const x = Math.random() * limits.rangeX + limits.minX;
		const y = Math.random() * limits.rangeY + limits.minY;

		switch (i) {
			case 0: limits.minX = 1; break; 
			case 1: limits.minY = 1; break;
			case 2: limits.minX = -1.5; break;
		}

		return THREE.Vector3(x, y, 1);
	}).map(function(pt) {
		return pt.clone().multiplyScalar(scale).setZ(1);
	}).map(function(pt) {
		const x = parseFloat(pt.x.toFixed(2)), 
					y = parseFloat(pt.y.toFixed(2));
		return point(x, y);
	}).map(function(pt) {
		return pt.clone().applyAxisAngle(zAxis, rotateAngle);
	});
}

function findLineEquations(points, testPoint) {
	function coefficients(p1, p2) {
		return p1.clone().cross(p2);
	}
	
	const lines = [];
	for (var i = 0; i < points.length; i++) {
		const fst = i;
		const snd = (i + 1) % points.length;
		const coeffs = coefficients(points[fst], points[snd])

		lines.push(coeffs);
	}

	return {
		lineEquations: lines.map(function(line) { return line; }),
		lineEquationsNormalized: lines.map(function(line) { 
			return line.clone().normalize(); 
		})
	};
}

function isMinimumDistanceSatisfied(lines, point, epsilonSq) {
	return lines.map(function(line) {
		const lineDotPoint = 1.0 * line.clone().dot(point);
		return (lineDotPoint * lineDotPoint) / (line.x * line.x + line.y * line.y);
	}).filter(function(distanceSq) {
		return distanceSq < epsilonSq;
	}).length == 0;
}

// dokle god je epsilonSq malen, ovo ce se izvesti u razumnom vremenu.
function getTestPoint(lines, maxAxis, epsilonSq) {
	var testPoint = randomPoint(maxAxis);
	
	while (!isMinimumDistanceSatisfied(lines, testPoint, epsilonSq)) {
		testPoint = randomPoint(maxAxis);
	}

	return testPoint;
}

function getMaxValue(points) {
	return points.map(function(pt) { return Math.max(Math.abs(pt.x), Math.abs(pt.y)); })
		.reduce(function(a, b) { return Math.max(a, b); });
}

function questionInitialize(questionConfig) {
	const numPts = questionConfig.polygon;
	const minScale = questionConfig.minPolygonScale;
	const maxScale = questionConfig.maxPolygonScale;
	const maxPointScale = questionConfig.maxPointScale;
	const minPointToLineDistance = 1;

	const pts = generateRandomPoints(numPts, minScale, maxScale);
  const solved = findLineEquations(pts);
	const testPoint = getTestPoint(solved.lineEquations, maxPointScale, minPointToLineDistance);

	const delta = 4;
	const maxAxis = Math.round(getMaxValue(pts.concat([testPoint])) + delta);

	solved.relationships = solved.lineEquations.map(function(line) { 
		return Math.sign(line.clone().dot(testPoint)); 
	});

  userData.question = {
		correct: solved,
		points: pts,
		testPoint: testPoint,
		maxAxis: maxAxis,
		canvasWidth: questionConfig.canvasWidth,
		canvasHeight: questionConfig.canvasHeight
	};

  userData.questionState = { };
}

function getComputedProperties() {
	const extractXY = function(pt) { return { x: pt.x.toFixed(2), 
																						y: pt.y.toFixed(2) }; };
	const toStr = function(pt) { return '(' + pt.x + ', ' + pt.y + ')'; };

  return { 
		points: userData.question.points.map(extractXY).map(toStr).toString(),
		testPoint: toStr(extractXY(userData.question.testPoint))
	};
}

function createEvaluationObject(correctness, solved) {
	return { correctness: correctness, solved: solved };
}

function questionEvaluate() {
	const user = userData.questionState;
	const solution = userData.question.correct;

	userData.correctQuestionState = [];
	userData.isSubmitted = true;

	var correct = 0;
	var solved = false;

	for (var i = 0; i < user.length; i++) {
		const row = new THREE.Vector3(user[i][0], user[i][1], user[i][2]).normalize(); 
		const correctEquation = solution.lineEquationsNormalized[i];
		const correctRelationship = solution.relationships[i];

		const nonNormalizedEquations = solution.lineEquations[i];
		userData.correctQuestionState.push([ nonNormalizedEquations.x, 
			nonNormalizedEquations.y, 
			nonNormalizedEquations.z, 
			correctRelationship ]);

		const epsilon = 0.1;
		correct += deq(row.x, correctEquation.x, epsilon);
		correct += deq(row.y, correctEquation.y, epsilon);
		correct += deq(row.z, correctEquation.z, epsilon);

		if (user[i][3] && correctRelationship == user[i][3])
			correct++;

		if (row.x || row.y || row.z || user[i][3]) solved = true;
	}

	const numEntriesPerRow = 4;
	const total = user.length * numEntriesPerRow;
  return createEvaluationObject(correct / total, solved);
}

function exportCommonState() {
	const common = copy(userData.question);
	common.correct = undefined;

	return common;
}

function exportUserState(stateType) {
  if (stateType == "USER") {
    return { qs: userData.questionState, 
						 disabled: userData.isSubmitted };
  }
  
  if (stateType == "CORRECT") {
    return { qs: userData.correctQuestionState, 
						 disabled: true };
  }

  return {};
}

function exportEmptyState() {
  return { qs: {} };
}

function importQuestionState(data) {
  userData.questionState = data;
}
