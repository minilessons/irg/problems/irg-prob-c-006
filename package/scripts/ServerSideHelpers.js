
const sign = Math.sign;

function copy(src) {
	const dst = {};
	Object.keys(src).forEach(function(k) { dst[k] = src[k]; });
	return dst;
}

function range(n) {
	return Array.apply(null, Array(n))
		.map(function (_, i) { return i; });
}

function doubleEquals(fst, snd, epsilon) { 
	return Math.abs(fst - snd) < epsilon;
}

const deq = doubleEquals;

function point(x, y) {
	return new THREE.Vector3(x, y, 1);
}

function line(pt1, pt2) {
	return [pt1, pt2];
}

function randomSign() {
	return sign(Math.random() - 0.5);
}

function random(limit) {
	return Math.round(Math.random() * limit);
}

function randomPoint(limit) {
	return point(randomSign() * random(limit), 
							 randomSign() * random(limit));
}

function distanceSq(vector) {
	return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
}
