
const ce = (e) => document.createElement(e);

function wrapTd(el) {
	const td = ce("td");
	td.appendChild(el);
	return td;
}

function createNameElement(pointName) {
	const div = ce("div");
	div.setAttribute("class", "form-control");
	div.setAttribute("disabled", true);
	div.innerHTML = pointName;
	return div;
}

function createInput(inputName) {
	const input = ce("input");
	input.setAttribute("type", "number");
	input.setAttribute("class", "form-control");
	return input;
}

// choices: lista tupleova oblika [value, string]
function createChoice(choices) {
	const createOption = (display, value) => {
		const o = ce("option");
		o.innerHTML = display;
		o.setAttribute("value", value);
		return o;
	}

	const sel = ce("select");
	sel.setAttribute("name", "selectbasic"); 
	sel.setAttribute("class", "form-control");
	sel.setAttribute("style", "width: 100%;");
	
	const empty = ["", ""]; 
	[empty].concat(choices)
		.map((strVal) => createOption(strVal[1], strVal[0]))
		.forEach(el => sel.appendChild(el));

	return sel;
}

