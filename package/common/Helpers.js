
function vec2(x, y) {
    return new THREE.Vector2(x, y);
}

function vec3(x, y, z) {
    return new THREE.Vector3(x, y, z);
}

function toVec3(vec2) {
    return new THREE.Vector3(vec2.x, vec2.y, 1);
}

function toVec2(vec3) {
    return vec2(vec3.x, vec3.y);
}

function pushAsVec3(geometry, vec2) {
    geometry.vertices.push(toVec3(vec2));
}

function pointLocation(point) {
		if(!point.geometry.boundingSphere)
			point.geometry.calculateBoundingSphere();

    return point.geometry.boundingSphere.center.clone();
}

function point(loc, materialProperties) {
    const RADIUS = 0.20;
    const N_SEGMENTS = 10;

    const material = new THREE.MeshBasicMaterial(materialProperties);
    const geometry = new THREE.CircleGeometry(RADIUS, N_SEGMENTS);

    geometry.translate(loc.x, loc.y, DEFAULT_Z_2D);
    geometry.computeFaceNormals();
    
    return new THREE.Mesh(geometry, material);
}

function arrow(from, to, color) {
    var from = toVec3(from);
    var to = toVec3(to);

    var direction = to.clone().sub(from);
    var length = direction.length();

    return new THREE.ArrowHelper(direction.normalize(), from, length, color, .5, .5);
}

