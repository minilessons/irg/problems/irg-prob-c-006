
function makeSelected(object) {
    if (!object) return object;

    object.children.forEach(line => {
        line.material.color = new THREE.Color(LINE_COLOR_SELECTED);
        line.material.needsUpdate = true;
    });

    return object;
}

function makeUnselected(object) {
    if (!object) return object;

    object.children.forEach(line => {
        line.material.color = new THREE.Color(LINE_COLOR_UNSELECTED);
        line.material.needsUpdate = true;
    });
    
    return object;
}

class Selection {

    constructor(elementsToIntersectProvider, 
								mapper, 
								preCallback, 
								postCallback, 
								intersectionFinder) {
        this.selectedLine = undefined;
        
        this.elementsToIntersectProvider = elementsToIntersectProvider;
        this.intersectionFinder = intersectionFinder;
        this.mapper = mapper;
        this.preCallback = preCallback;
        this.postCallback = postCallback;

        this.onMousemove = this.onMousemove.bind(this);
    }

    _findClosestPoint(intersections, mouseWorld) {
        var intersect = undefined;
        var intersectD = undefined;
        
        for (let i in intersections) {
            const currIntersection = intersections[i];
            const distance = mouseWorld.distanceTo(currIntersection.point);

            if (!intersectD || distance < intersectD) {
                intersect = currIntersection;
                intersectD = distance;
            }
        }

        return intersect;
    }

    onMousemove(e) {
        this.preCallback();
        this.selectedLine = makeUnselected(this.selectedLine);

        const mouseWorld = this.intersectionFinder.worldCoordinates(e.clientX, e.clientY);
        const allIntersects = this.intersectionFinder
					.getIntersections(e.clientX, e.clientY, this.elementsToIntersectProvider());
        const intersect = this._findClosestPoint(this.mapper(allIntersects), mouseWorld);
        
        if (intersect) {
            this.selectedLine = makeSelected(intersect.object.parent);
            this.postCallback(intersect, e.layerX, e.layerY, e);
        }
    }
}
